package com.tgam.cycle.util;

import org.springframework.hateoas.Identifiable;
import org.springframework.hateoas.ResourceSupport;

public interface ResourceDisassembler<T extends ResourceSupport, D extends Identifiable<?>> {
    D fromResource(T resource);
}
