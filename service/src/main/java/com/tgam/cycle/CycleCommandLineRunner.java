package com.tgam.cycle;

import com.tgam.cycle.hub.*;
import com.tgam.cycle.hub.site.Site;
import com.tgam.cycle.hub.site.SiteRepository;
import com.tgam.cycle.hub.site.SiteStatus;
import com.tgam.cycle.hub.site.SiteStatusRepository;
import com.tgam.cycle.hub.user.User;
import com.tgam.cycle.hub.user.UserRepository;
import com.tgam.cycle.hub.user.UserStatus;
import com.tgam.cycle.hub.user.UserStatusRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import sun.security.krb5.internal.ccache.CredentialsCache;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class CycleCommandLineRunner implements CommandLineRunner {
    private static final Logger log = LoggerFactory.getLogger(CycleCommandLineRunner.class);

    @Value("${cycle.env}")
    private String cycleEnv;

    @Autowired
    private SiteRepository siteRepository;

    @Autowired
    private SiteStatusRepository siteStatusRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserStatusRepository userStatusRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RoleService roleService;

    @Override
    public void run(String... args) {
        log.info("cycle.env: " + this.cycleEnv);

        if (this.cycleEnv.contains("nodb")) {
            Site site = new Site();
            site.setTitle("My Site");
            site.setDescription("This is my site.");

            SiteStatus status = new SiteStatus();
            status.setName("Active");
            status.setDescription("Status for active sites");
            status.setReadOnly(false);
            status = siteStatusRepository.save(status);
            site.setStatus(status);

            siteRepository.save(site);

            UserStatus userStatus = new UserStatus();
            userStatus.setName("Active");
            userStatus.setDescription("Status for active users");
            userStatus.setReadOnly(false);
            userStatus = userStatusRepository.save(userStatus);

            User user = new User();
            user.setUsername("frood");
            user.setPassword(passwordEncoder.encode("password"));
            user.setDisplayName("Fred Rood");
            user.setEmail("frood@example.com");
            user.setDeactivated(false);
            user.setSystemRole(Role.ADMIN);
            user.setStatus(userStatus);

            roleService.addUserRole(user, site, Role.ADMIN);

            Set<Site> sites = new HashSet<>();
            sites.add(site);

            user.setSites(sites);

            userRepository.save(user);

            user = new User();
            user.setUsername("norm");
            user.setPassword(passwordEncoder.encode("qwerty1234"));
            user.setDisplayName("Norm User");
            user.setEmail("norm@example.com");
            user.setDeactivated(false);
            user.setSystemRole(Role.NORMAL);
            user.setStatus(userStatus);

            roleService.addUserRole(user, site, Role.NORMAL);

            user.setSites(sites);

            userRepository.save(user);

            List<Site> siteList = new ArrayList<>();

            for (int i = 0; i < 30; i++) {
                site = new Site();
                site.setTitle("Site No " + i);
                site.setDescription("This is the description for Site No " + i);

                siteList.add(site);
            }

            siteRepository.save(siteList);


        }
    }
}
