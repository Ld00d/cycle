package com.tgam.cycle.frame;

import javax.persistence.*;

@Entity
public class Requirement {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String name;
    private String description;
    @ManyToOne
    private Requirement parentRequirement;
    @ManyToOne
    private Project project;
    @ManyToOne
    private RequirementStatus status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Requirement getParentRequirement() {
        return parentRequirement;
    }

    public void setParentRequirement(Requirement parentRequirement) {
        this.parentRequirement = parentRequirement;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public RequirementStatus getStatus() {
        return status;
    }

    public void setStatus(RequirementStatus status) {
        this.status = status;
    }
}
