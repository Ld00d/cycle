package com.tgam.cycle.auth;

public class Constants {
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String AUTH_HEADER = "Authorization";
}
