package com.tgam.cycle.hub.site;

import com.tgam.cycle.util.ResourceDisassembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.IdentifiableResourceAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class SiteResourceAssembler
        extends IdentifiableResourceAssemblerSupport<Site, SiteResource>
        implements ResourceDisassembler<SiteResource, Site> {

    @Autowired
    private SiteStatusResourceAssembler siteStatusResourceAssembler;

    public SiteResourceAssembler() {
        super(SiteController.class, SiteResource.class);
    }

    @Override
    public SiteResource toResource(Site entity) {
        SiteResource siteResource = createResource(entity);

        siteResource.setInternalId(entity.getId());
        siteResource.setTitle(entity.getTitle());
        siteResource.setDescription(entity.getDescription());
        SiteStatus status = entity.getStatus();
        if (status != null) {
            siteResource.setStatus(siteStatusResourceAssembler.toResource(status));
        }

        return siteResource;
    }

    @Override
    public Site fromResource(SiteResource resource) {
        Site site = null;

        if (resource != null) {
            site = new Site();
            site.setId(resource.getInternalId());
            site.setTitle(resource.getTitle());
            site.setDescription(resource.getDescription());

            SiteStatus status = siteStatusResourceAssembler.fromResource(resource.getStatus());

            site.setStatus(status);
        }

        return site;
    }
}
