package com.tgam.cycle.hub;

import com.tgam.cycle.hub.site.SiteController;
import com.tgam.cycle.hub.site.SiteStatusController;
import com.tgam.cycle.hub.user.UserController;
import com.tgam.cycle.hub.user.UserStatusController;
import org.springframework.data.rest.webmvc.RepositoryLinksResource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class HubLinkProcessor implements ResourceProcessor<RepositoryLinksResource> {

    @Override
    public RepositoryLinksResource process(RepositoryLinksResource resource) {
        resource.add(
                linkTo(methodOn(UserController.class)
                        .getCurrent(null))
                        .withRel("current-user"));

        resource.add(
                linkTo(methodOn(UserController.class)
                        .searchByUsername(null, null, null, null))
                        .withRel("users-search-by-username"));

        resource.add(
                linkTo(methodOn(SiteController.class)
                        .searchByTitle(null, null, null, null))
                        .withRel("sites-search-by-title"));

        resource.add(
                linkTo(methodOn(SiteController.class)
                        .get(null))
                        .withRel("site-by-id"));

        resource.add(
                linkTo(methodOn(SiteStatusController.class)
                        .get(null))
                        .withRel("site-status-by-id"));

        resource.add(
                linkTo(methodOn(SiteStatusController.class)
                        .searchByName(null, null, null, null))
                        .withRel("site-statuses-search-by-name"));

        resource.add(
                linkTo(methodOn(UserController.class)
                        .get(null))
                        .withRel("user-by-id"));

        resource.add(
                linkTo(methodOn(UserController.class)
                        .getForAdmin(null))
                        .withRel("user-admin-by-id"));

        resource.add(
                linkTo(methodOn(UserStatusController.class)
                        .get(null))
                        .withRel("user-status-by-id"));

        resource.add(
                linkTo(methodOn(UserStatusController.class)
                        .searchByName(null, null, null, null))
                        .withRel("user-statuses-search-by-name"));

        return resource;
    }
}