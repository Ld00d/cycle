package com.tgam.cycle.hub.site;

import com.tgam.cycle.hub.StatusResourceAssembler;
import org.springframework.stereotype.Component;

@Component
public class SiteStatusResourceAssembler extends StatusResourceAssembler<SiteStatus, SiteStatusResource> {
    public SiteStatusResourceAssembler() {
        super(SiteStatusController.class, SiteStatusResource.class, SiteStatus.class);
    }
}
