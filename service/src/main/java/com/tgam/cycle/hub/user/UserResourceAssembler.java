package com.tgam.cycle.hub.user;

import com.tgam.cycle.hub.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.IdentifiableResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class UserResourceAssembler
        extends IdentifiableResourceAssemblerSupport<User, UserResource> {

    @Autowired
    private UserStatusResourceAssembler userStatusResourceAssembler;

    public UserResourceAssembler() {
        super(UserController.class, UserResource.class);
    }

    @Override
    public UserResource toResource(User entity) {
        UserResource userResource = createResource(entity);
        userResource.setInternalId(entity.getId());
        userResource.setUsername(entity.getUsername());
        userResource.setCreated(entity.getCreated());
        userResource.setUpdated(entity.getUpdated());
        userResource.setDisplayName(entity.getDisplayName());

        if (entity.getStatus() != null) {
            UserStatusResource userStatusResource = userStatusResourceAssembler.toResource(entity.getStatus());
            userResource.setStatus(userStatusResource);
        }

        userResource.setSystemAdmin(entity.getSystemRole() == Role.ADMIN);

        Link sitesLink = linkTo(methodOn(UserController.class).getUserSites(entity.getId())).withRel("sites");
        userResource.add(sitesLink);

        return userResource;
    }
}
