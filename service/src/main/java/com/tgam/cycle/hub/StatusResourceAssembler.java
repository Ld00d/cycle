package com.tgam.cycle.hub;

import com.tgam.cycle.util.ResourceDisassembler;

import org.springframework.beans.BeanUtils;
import org.springframework.hateoas.mvc.IdentifiableResourceAssemblerSupport;


public class StatusResourceAssembler<T extends Status, D extends StatusResource>
        extends IdentifiableResourceAssemblerSupport<T, D>
        implements ResourceDisassembler<D, T> {


    private Class<T> statusType;


    public StatusResourceAssembler(Class<?> controllerClass, Class<D> resourceType, Class<T> statusType) {
        super(controllerClass, resourceType);
        this.statusType = statusType;
    }

    @Override
    public D toResource(T entity) {
        D siteStatusResource = createResource(entity);

        siteStatusResource.setInternalId(entity.getId());
        siteStatusResource.setName(entity.getName());
        siteStatusResource.setDescription(entity.getDescription());
        siteStatusResource.setReadOnly(entity.getReadOnly());

        return siteStatusResource;
    }

    @Override
    public T fromResource(D resource) {
        T status = null;

        if (resource != null) {
            status = BeanUtils.instantiate(statusType);
            status.setId(resource.getInternalId());
            status.setName(resource.getName());
            status.setDescription(resource.getDescription());
            status.setReadOnly(resource.getReadOnly());
        }

        return status;
    }
}
