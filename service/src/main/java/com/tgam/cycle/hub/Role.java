package com.tgam.cycle.hub;

import javax.persistence.*;
import java.util.Set;

public enum Role {
    ADMIN,
    NORMAL,
    DEACTIVATED
}
