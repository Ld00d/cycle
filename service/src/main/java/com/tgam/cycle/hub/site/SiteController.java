package com.tgam.cycle.hub.site;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping("/sites")
public class SiteController {

    @Autowired
    private SiteRepository siteRepository;
    @Autowired
    private SiteResourceAssembler siteResourceAssembler;

    @GetMapping("/{siteId}")
    @PostAuthorize("hasPermission(returnObject, 'read')")
    public SiteResource get(@PathVariable Long siteId) {
        Site site = siteRepository.findOne(siteId);
        if (site == null) {
            throw new ResourceNotFoundException();
        }
        return siteResourceAssembler.toResource(site);
    }

    @GetMapping
    @SuppressWarnings("unchecked")
    public PagedResources<?> get(Pageable pageable, PagedResourcesAssembler pagedResourcesAssembler) {
        Page<Site> sites = siteRepository.findAll(pageable);
        return pagedResourcesAssembler.toResource(sites, siteResourceAssembler);
    }

    @GetMapping("/search-by-title/{title}")
    public PagedResources<?> searchByTitle(
            @PathVariable("title") String title,
            @RequestParam("page") Integer page,
            @RequestParam("size") Integer size,
            PagedResourcesAssembler<Site> pagedResourcesAssembler) {


        if (page == null) {
            page = 0;
            size = 20;
        }

        if (size == null) {
            size = 20;
        }

        Pageable pageable = new PageRequest(page, size);

        Page<Site> sites = siteRepository.findByTitleLike(title.toLowerCase(), pageable);
        return pagedResourcesAssembler.toResource(sites, siteResourceAssembler);

    }

    @PostMapping
    public ResponseEntity<?> create(@RequestBody SiteResource siteResource) {
        Site site = siteResourceAssembler.fromResource(siteResource);
        site = siteRepository.save(site);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(site.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping("{siteId}")
    public ResponseEntity<?> update(
            @PathVariable Long siteId,
            @RequestBody SiteResource siteResource) {

        Site site = siteResourceAssembler.fromResource(siteResource);

        siteRepository.save(site);
        return ResponseEntity.noContent().build();
    }

}
