package com.tgam.cycle.hub.site;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SiteRepository extends JpaRepository<Site, Long> {

    @Query("SELECT s FROM Site s WHERE lower(s.title) LIKE %:searchTerm%")
    Page<Site> findByTitleLike(@Param("searchTerm") String searchTerm, Pageable pageable);

    Page<Site> findByTitleContainingIgnoreCase(String title, Pageable pageable);
}
