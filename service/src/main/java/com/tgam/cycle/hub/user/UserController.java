package com.tgam.cycle.hub.user;

import com.tgam.cycle.hub.site.SiteResource;
import com.tgam.cycle.hub.site.SiteResourceAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.security.Principal;
import java.time.LocalDate;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserResourceAssembler userResourceAssembler;
    @Autowired
    private SiteResourceAssembler siteResourceAssembler;
    @Autowired
    private UserAdminResourceAssembler userAdminResourceAssembler;


    @GetMapping("/{userId}")
    public UserResource get(@PathVariable Long userId) {
        User user = userRepository.findOne(userId);
        if (user == null) {
            throw new ResourceNotFoundException();
        }
        return userResourceAssembler.toResource(user);
    }


    @GetMapping("/current")
    public UserResource getCurrent(Principal principal) {
        User user = userRepository.findByUsername(principal.getName());
        if (user == null) {
            throw new ResourceNotFoundException();
        }
        return userResourceAssembler.toResource(user);
    }

    @GetMapping
    public PagedResources<UserResource> get(Pageable pageable, PagedResourcesAssembler<User> pagedResourcesAssembler) {
        Page<User> users = userRepository.findAll(pageable);
        return pagedResourcesAssembler.toResource(users, userResourceAssembler);
    }

    @GetMapping("/{userId}/sites")
    public Resources<SiteResource> getUserSites(@PathVariable Long userId) {
        User user = userRepository.findOne(userId);
        if (user == null) {
            throw new ResourceNotFoundException();
        }

        List<SiteResource> siteResources = siteResourceAssembler.toResources(user.getSites());

        return new Resources<>(
                siteResources,
                linkTo(methodOn(UserController.class).getUserSites(userId))
                        .withSelfRel());
    }

    @GetMapping("/search-by-username/{username}")
    public PagedResources<UserResource> searchByUsername(
            @PathVariable("username") String username,
            @RequestParam("page") Integer page,
            @RequestParam("size") Integer size,
            PagedResourcesAssembler<User> pagedResourcesAssembler) {
        if (page == null) {
            page = 0;
            size = 20;
        }

        if (size == null) {
            size = 20;
        }

        Pageable pageable = new PageRequest(page, size);

        Page<User> users = userRepository.findByUsernameContainingIgnoreCase(username.toLowerCase(), pageable);

        return pagedResourcesAssembler.toResource(users, userResourceAssembler);
    }


    @GetMapping("/{userId}/admin")
    public UserAdminResource getForAdmin(@PathVariable Long userId) {
        User user = userRepository.findOne(userId);
        if (user == null) {
            throw new ResourceNotFoundException();
        }

        return userAdminResourceAssembler.toResource(user);
    }


    @PostMapping
    public ResponseEntity<?> create(@RequestBody UserAdminResource userAdminResource) {

        if (!StringUtils.isEmpty(userAdminResource.getPassword())) {
            userAdminResource.setPassword(passwordEncoder.encode(userAdminResource.getPassword()));
        }

        User user = userAdminResourceAssembler.fromResource(userAdminResource);
        user = userRepository.save(user);


        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}/admin")
                .buildAndExpand(user.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody UserAdminResource userAdminResource) {
        if (!StringUtils.isEmpty(userAdminResource.getPassword())) {
            //not going to allow password changes here
            userAdminResource.setPassword(null);
        }

        User user = userAdminResourceAssembler.fromResource(userAdminResource);
        userRepository.save(user);

        return ResponseEntity.noContent().build();
    }


    @PutMapping("/{userId}/password")
    public ResponseEntity<?> updatePassword(
            @PathVariable("userId") Long userId,
            @RequestBody PasswordUpdate passwordUpdate) {
        User user = userRepository.getOne(userId);

        if (user == null) {
            throw new ResourceNotFoundException();
        }

        String oldPass = passwordEncoder.encode(passwordUpdate.getOldPassword());

        if (user.getPassword().equals(oldPass)) {
            user.setPassword(passwordEncoder.encode(passwordUpdate.getNewPassword()));
            userRepository.save(user);

            return ResponseEntity.noContent().build();
        } else {
            throw new BadCredentialsException("Old password does not match for user.");
        }
    }

    @PutMapping("/{userId}/deactivate")
    public ResponseEntity<?> deactivateUser(@PathVariable("userId") Long userId) {
        User user = userRepository.getOne(userId);

        if (user == null) {
            throw new ResourceNotFoundException();
        }

        user.setDeactivated(true);
        userRepository.save(user);

        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{userId}/activate")
    public ResponseEntity<?> activateUser(@PathVariable("userId") Long userId) {
        User user = userRepository.getOne(userId);

        if (user == null) {
            throw new ResourceNotFoundException();
        }

        user.setDeactivated(false);
        userRepository.save(user);

        return ResponseEntity.noContent().build();
    }




}
