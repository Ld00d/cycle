package com.tgam.cycle.hub.site;

import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

@Relation(collectionRelation = "sites")
public class SiteResource extends ResourceSupport {
    private Long internalId;
    private String title;
    private String description;
    private SiteStatusResource status;

    public Long getInternalId() {
        return internalId;
    }

    public void setInternalId(Long internalId) {
        this.internalId = internalId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SiteStatusResource getStatus() {
        return status;
    }

    public void setStatus(SiteStatusResource status) {
        this.status = status;
    }
}
