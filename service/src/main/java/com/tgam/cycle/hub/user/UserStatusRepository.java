package com.tgam.cycle.hub.user;

import com.tgam.cycle.hub.StatusRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "user-statuses", path = "/user-statuses")
public interface UserStatusRepository extends StatusRepository<UserStatus> {
}
