package com.tgam.cycle.hub.user;

import com.tgam.cycle.hub.StatusResourceAssembler;
import org.springframework.stereotype.Component;

@Component
public class UserStatusResourceAssembler extends StatusResourceAssembler<UserStatus, UserStatusResource> {
    public UserStatusResourceAssembler() {
        super(UserStatusController.class, UserStatusResource.class, UserStatus.class);
    }
}
