package com.tgam.cycle.hub;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

public abstract class StatusController<T extends Status, S extends StatusResource> {

    private StatusRepository<T> statusRepository;

    private StatusResourceAssembler<T, S> statusResourceAssembler;

    public StatusController(
            StatusRepository<T> statusRepository,
            StatusResourceAssembler<T, S> statusResourceAssembler) {
        this.statusRepository = statusRepository;
        this.statusResourceAssembler = statusResourceAssembler;
    }

    @GetMapping
    public PagedResources<S> get(
            @RequestParam(value = "page", defaultValue = "0") Integer pageParam,
            @RequestParam(value = "size", defaultValue = "0") Integer sizeParam,
            PagedResourcesAssembler<T> pagedResourcesAssembler) {
        Pageable pageable;
        Page<T> page;

        if (sizeParam > 0) {
            pageable = new PageRequest(pageParam, sizeParam);
            page = statusRepository.findAll(pageable);
        } else {
            List<T> all = statusRepository.findAll();
            pageable = new PageRequest(0, all.size());
            page = new PageImpl<>(all, pageable, all.size());
        }

        return pagedResourcesAssembler.toResource(page, statusResourceAssembler);
    }

    @GetMapping("/{id}")
    public ResponseEntity<S> get(@PathVariable("id") Long statusId) {
        T status = statusRepository.findOne(statusId);

        if (status == null) {
            ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(statusResourceAssembler.toResource(status));
    }

    @GetMapping("/search-by-name/{name}")
    public PagedResources<?> searchByName(
            @PathVariable("name") String name,
            @RequestParam("page") Integer page,
            @RequestParam("size") Integer size,
            @SuppressWarnings("ConstantConditions") PagedResourcesAssembler<T> pagedResourcesAssembler) {
        if (page == null) {
            page = 0;
            size = 20;
        }

        if (size == null) {
            size = 20;
        }

        Pageable pageable = new PageRequest(page, size);

        Page<T> statuses = statusRepository.findByNameContainingIgnoreCase(name, pageable);
        return pagedResourcesAssembler.toResource(statuses, statusResourceAssembler);
    }

    @PostMapping
    public ResponseEntity<?> create(@RequestBody S statusResource) {
        T status = statusResourceAssembler.fromResource(statusResource);
        status = statusRepository.save(status);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(status.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(
            @PathVariable("id") Long statusId,
            @RequestBody S statusResource) {

        if (statusRepository.findOne(statusId) == null) {
            return ResponseEntity.notFound().build();
        }

        T status = statusResourceAssembler.fromResource(statusResource);

        statusRepository.save(status);
        return ResponseEntity.noContent().build();
    }
}
