package com.tgam.cycle.hub;

import org.springframework.hateoas.ResourceSupport;

public abstract class StatusResource extends ResourceSupport {
    private Long internalId;
    private String name;
    private String description;
    private Boolean isReadOnly;

    public Long getInternalId() {
        return internalId;
    }

    public void setInternalId(Long internalId) {
        this.internalId = internalId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getReadOnly() {
        return isReadOnly;
    }

    public void setReadOnly(Boolean readOnly) {
        isReadOnly = readOnly;
    }
}
