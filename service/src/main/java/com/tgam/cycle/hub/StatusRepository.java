package com.tgam.cycle.hub;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface StatusRepository<T extends Status> extends JpaRepository<T, Long> {
    Page<T> findByNameContainingIgnoreCase(String name, Pageable pageable);
}
