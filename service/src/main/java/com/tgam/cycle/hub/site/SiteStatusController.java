package com.tgam.cycle.hub.site;


import com.tgam.cycle.hub.StatusController;
import com.tgam.cycle.hub.StatusRepository;
import com.tgam.cycle.hub.StatusResourceAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


import java.net.URI;
import java.util.List;


@RestController
@RequestMapping("/site-statuses")
public class SiteStatusController extends StatusController<SiteStatus, SiteStatusResource> {

    @Autowired
    public SiteStatusController(SiteStatusRepository statusRepository, SiteStatusResourceAssembler statusResourceAssembler) {
        super(statusRepository, statusResourceAssembler);
    }
}
