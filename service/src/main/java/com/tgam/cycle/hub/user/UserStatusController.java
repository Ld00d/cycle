package com.tgam.cycle.hub.user;


import com.tgam.cycle.hub.StatusController;
import com.tgam.cycle.hub.StatusRepository;
import com.tgam.cycle.hub.StatusResourceAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping("/user-statuses")
public class UserStatusController extends StatusController<UserStatus, UserStatusResource> {

    @Autowired
    public UserStatusController(
            UserStatusRepository statusRepository,
            UserStatusResourceAssembler statusResourceAssembler) {
        super(statusRepository, statusResourceAssembler);
    }
}
