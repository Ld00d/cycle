package com.tgam.cycle.hub.site;

import com.tgam.cycle.hub.StatusResource;
import org.springframework.hateoas.core.Relation;

@Relation(collectionRelation = "site-statuses")
public class SiteStatusResource extends StatusResource {

}
