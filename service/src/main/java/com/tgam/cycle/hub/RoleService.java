package com.tgam.cycle.hub;

import com.tgam.cycle.hub.user.User;
import com.tgam.cycle.hub.user.UserRepository;
import com.tgam.cycle.hub.user.UserRole;
import com.tgam.cycle.hub.user.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Identifiable;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class RoleService {
    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private UserRepository userRepository;

    public void addUserRole(User user, Identifiable<Long> domain, Role role) {
        UserRole userRole = new UserRole();
        userRole.setDomain(domain.getClass().getName());
        userRole.setDomainId(domain.getId());
        userRole.setRole(role);

        userRole = userRoleRepository.save(userRole);

        Set<UserRole> userRoles = user.getRoles();

        if (userRoles == null) {
            userRoles = new HashSet<>();
            user.setRoles(userRoles);
        }

        userRoles.add(userRole);

        userRepository.save(user);
    }

}
