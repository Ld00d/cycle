package com.tgam.cycle.hub.user;

import com.tgam.cycle.util.ResourceDisassembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.IdentifiableResourceAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class UserAdminResourceAssembler
    extends IdentifiableResourceAssemblerSupport<User, UserAdminResource>
    implements ResourceDisassembler<UserAdminResource, User> {

    @Autowired
    private UserStatusResourceAssembler userStatusResourceAssembler;

    public UserAdminResourceAssembler() {
        super(UserController.class, UserAdminResource.class);
    }

    @Override
    public UserAdminResource toResource(User entity) {
        UserAdminResource resource = createResource(entity);
        resource.setInternalId(entity.getId());
        resource.setUsername(entity.getUsername());
        resource.setEmail(entity.getEmail());
        resource.setCreated(entity.getCreated());
        resource.setUpdated(entity.getUpdated());
        resource.setDisplayName(entity.getDisplayName());
        resource.setDeactivated(entity.getDeactivated());

        if (entity.getStatus() != null) {
            UserStatusResource userStatusResource = userStatusResourceAssembler.toResource(entity.getStatus());
            resource.setStatus(userStatusResource);
        }

        return resource;
    }

    @Override
    public User fromResource(UserAdminResource resource) {
        User user = new User();
        user.setId(resource.getInternalId());
        user.setUsername(resource.getUsername());
        user.setPassword(resource.getPassword());
        user.setEmail(resource.getEmail());
        user.setDisplayName(resource.getDisplayName());

        if (resource.getStatus() != null) {
            UserStatus userStatus = userStatusResourceAssembler.fromResource(resource.getStatus());
            user.setStatus(userStatus);
        }

        return user;
    }
}
