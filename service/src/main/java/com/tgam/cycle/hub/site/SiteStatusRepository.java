package com.tgam.cycle.hub.site;

import com.tgam.cycle.hub.StatusRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "site-statuses", path = "/site-statuses")
public interface SiteStatusRepository extends StatusRepository<SiteStatus> {

}
