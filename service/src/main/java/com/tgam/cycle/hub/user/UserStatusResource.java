package com.tgam.cycle.hub.user;

import com.tgam.cycle.hub.StatusResource;
import org.springframework.hateoas.core.Relation;

@Relation(collectionRelation = "user-statuses")
public class UserStatusResource extends StatusResource {
}
