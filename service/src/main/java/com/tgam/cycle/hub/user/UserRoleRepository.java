package com.tgam.cycle.hub.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "user-roles", path = "/user-roles")
public interface UserRoleRepository extends JpaRepository<UserRole, Long> {
}
