const defer = function() {
  let ret = {}

  let promise = new Promise(function(resolve, reject) {
    ret.resolve = resolve
    ret.reject = reject
  })

  ret.promise = promise

  return ret
}

export default defer
