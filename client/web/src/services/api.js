import axios from 'axios'
import store from '@/store'
import defer from '@/util/defer'
import urltemplate from 'url-template'
import _ from 'lodash'


class Api {

  constructor() {
    this.root = null
  }

  getRoot() {
    let d = defer()

    if (this.root) {
      d.resolve(this.root)
    } else {
      this.getLink('/api/').then(r => {
        this.root = r.data
        d.resolve(r.data)
      })
    }

    return d.promise
  }

  fromLink(link) {



  }

  get(ref, params) {
    return this.getRoot().then(
      r => {
        if (!r._links[ref]) {
          console.warn('Missing link', ref)
        }
        return this.getLink(r._links[ref].href, params)
      })
  }

  post(ref, data, params) {
    return this.getRoot().then(
      r => {
        return this.postLink(r._links[ref].href, data, params)
      })

  }

  put(ref, data, params) {
    return this.getRoot().then(
      r => {
        return this.putLink(r._links[ref].href, data, params)
      })
  }

  getLink(link, params) {
    params = params || {}

    link = this.parseTemplate(link, params)

    return axios.get(link, this.getRequestConfig()).catch(this.handleError)
  }

  postLink(link, data, params) {
    params = params || {}

    link = this.parseTemplate(link, params)

    return axios.post(link, data, this.getRequestConfig()).catch(this.handleError)
  }

  putLink(link, data, params) {
    params = params || {}

    link = this.parseTemplate(link, params)

    return axios.put(link, data, this.getRequestConfig()).catch(this.handleError)
  }

  handleError(error) {
    var err = _.pick(error.response, ['status', 'statusText'])

    if (err.status === 403) {
      //TODO: handle this and put up request for login
      store.dispatch('auth/logout')
    }

  }

  parseTemplate(url, params) {
    var parser = urltemplate.parse(url)
    return parser.expand(params)
  }

  getRequestConfig() {
    var authToken = this.getAuthToken()

    if (authToken) {
      return { headers: { 'Authorization': authToken } }
    }
    return {}
  }

  getAuthToken() {
    return store.getters['auth/authToken']
  }

}

const api = new Api()

export default api
