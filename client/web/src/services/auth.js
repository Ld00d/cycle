import api from './api'

class Auth {

  login(uname, pass) {
    return api.post('auth-login', {'username': uname, 'password': pass})
  }

}

const auth = new Auth()

export default auth
