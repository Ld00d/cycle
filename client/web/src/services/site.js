import api from './api'
import _ from 'lodash'

const PAGE_SZ = 10

class Site {
  getSites(page, size) {
    page = page || 0
    size = size || PAGE_SZ

    return api.get('sites', {page: page, size: size})
  }

  searchByTitle(title, page, size) {
    page = page || 0
    size = size || PAGE_SZ

    return api.get('sites-search-by-title', {title: title, page: page, size: size})
  }

  getSite(id) {
    return api.get('site-by-id', {siteId: id})
  }

  getSiteFromLink(link) {
    return api.getLink(link)
  }

  createSite(s) {
    return api.post('sites', s)
  }

  updateSite(s) {
    var selfLink = _.get(s, '_links.self.href')

    return api.putLink(selfLink, s)
  }
}

const site = new Site()

export default site
