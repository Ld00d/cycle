import api from './api'
import _ from 'lodash'

const PAGE_SZ = 10

class User {

  getCurrent() {
    return api.get('current-user')
  }

  getUsers(page, size) {
    page = page || 0
    size = size || PAGE_SZ

    return api.get('users', {
      page: page,
      size: size
    })
  }

  searchByUsername(username, page, size) {
    page = page || 0
    size = size || PAGE_SZ

    return api.get('users-search-by-username', {
      username: username,
      page: page,
      size: size
    })
  }

  getUser(id) {
    return api.get('user-by-id', {
      userId: id
    })
  }

  getUserForAdmin(id) {
    return api.get('user-admin-by-id', {
      userId: id
    })
  }

  getUserFromLink(link) {
    return api.getLink(link)
  }

  createUser(s) {
    return api.post('users', s)
  }

  updateUser(s) {
    var selfLink = _.get(s, '_links.self.href')

    return api.putLink(selfLink, s)
  }
}

const user = new User()

export default user
