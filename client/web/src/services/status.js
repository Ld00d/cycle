import api from './api'
import _ from 'lodash'

const PAGE_SZ = 10

class Status {

  getStatuses(type, page, size) {
    var ref = type + '-statuses'

    if (page || page === 0) {
      size = size || PAGE_SZ
      return api.get(ref, {page: page, size: size})
    } else {
      return api.get(ref)
    }
  }

  getStatus(type, id) {
    var ref = type + '-status-by-id'
    return api.get(ref, {id: id})
  }

  searchByName(type, name, page, size) {
    var ref = type + '-statuses-search-by-name'
    page = page || 0
    size = size || PAGE_SZ

    return api.get(ref, {name: name, page: page, size: size})
  }

  getStatusFromLink(link) {
    return api.getLink(link)
  }

  createStatus(type, s) {
    return api.post(type + '-statuses', s)
  }

  updateStatus(s) {
    var selfLink = _.get(s, '_links.self.href')
    return api.putLink(selfLink, s)
  }

}

const status = new Status()

export default status
