import Vue from 'vue'

(function() {
  function padLeftZero(value, digits) {
    if (digits === undefined) {
      digits = 2
    }

    value = '' + value
    return value.padStart(digits, '0')
  }

  function dateFormat(value) {
    return `${value.year}-${padLeftZero(value.monthValue)}-${padLeftZero(value.dayOfMonth)}`
  }

  Vue.filter('date-format', function (value) {
    if (!value) return ''

    return dateFormat(value)
  })

  Vue.filter('datetime-format', function (value) {
    if (!value) return ''

    return dateFormat(value) +
      ` ${padLeftZero(value.hour)}:${padLeftZero(value.minute)}:${padLeftZero(value.second)}`
  })

})()

