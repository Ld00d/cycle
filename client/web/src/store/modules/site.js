import siteSvc from '@/services/site'
import _ from 'lodash'


const state = {
  sites: [],
  pageNo: 0, //zero indexed
  pageCount: 0,
  loadedSite: {status: null},
  createdSite: {status: null}

}

const getters = {
  sites: state => state.sites,
  loadedSite: state => state.loadedSite,
  createdSite: state => state.createdSite,
  pageNo: state => state.pageNo,
  pageCount: state => state.pageCount,
  hasNextPage: state => state.pageNo + 1 < state.pageCount,
  hasPrevPage: state => state.pageNo > 0
}

function commitChangePage(commit, r) {
  if (r) {
    commit('changePage', {
      page: _.get(r, 'data.page'),
      sites: _.get(r, 'data._embedded.sites', [])
    })
  }
}

function goToPage(pageNo, title, commit) {
  if (!title) {
    siteSvc.getSites(pageNo).then(r => {
      commitChangePage(commit, r)
    })
  } else {
    siteSvc.searchByTitle(title, pageNo).then(r => {
      commitChangePage(commit, r)
    })
  }
}

const actions = {
  loadSite({commit}, id) {
    siteSvc.getSite(id).then(r => {
      if (r) {
        commit('loadSite', r.data)
      }

    })
  },

  firstPage({commit}, title) {
    goToPage(0, title, commit)
  },
  nextPage({commit, getters}, title) {
    const nextPage = getters.pageNo + 1
    goToPage(nextPage, title, commit)
  },
  prevPage({commit, getters}, title) {
    const prevPage = getters.pageNo - 1
    goToPage(prevPage, title, commit)
  },
  createSite({commit}, s) {
    siteSvc.createSite(s).then(r => {
      var link = _.get(r, 'headers.location')
      if (link) {
        siteSvc.getSiteFromLink(link)
          .then(s => {
            if (s) {
              commit('loadCreatedSite', s.data)
            }

          })
      }
    })
  },
  updateSite({commit}, s) {
    siteSvc.updateSite(s).then(r => {
      // TODO: toaster?
    })
  },
  resetLoadedSite({commit}) {
    commit('resetLoadedSite')
  },
  resetCreatedSite({commit}) {
    commit('resetCreatedSite')
  }

}

const mutations = {
  changePage(state, {page, sites}) {
    state.pageNo = page.number
    state.pageCount = page.totalPages
    state.sites = sites
  },
  loadSite(state, site) {
    state.loadedSite = site
  },
  loadCreatedSite(state, site) {
    state.createdSite = site
  },
  resetLoadedSite(state) {
    state.loadedSite = {status: null}
  },
  resetCreatedSite(state) {
    state.createdSite = {status: null}
  }

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
