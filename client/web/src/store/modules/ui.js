const state = {
  viewTitle: ''
}

const getters = {
  viewTitle: state => state.viewTitle
}

const actions = {
  updateViewTitle({commit}, viewTitle) {
    commit('viewTitle', viewTitle)
    document.title = 'Cycle | ' + viewTitle
  }
}

const mutations = {
  viewTitle(state, viewTitle) {
    state.viewTitle = viewTitle
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
