import userSvc from '@/services/user'
import _ from 'lodash'

const state = {
  userInfo: {},
  users: [],
  pageNo: 0, //zero indexed
  pageCount: 0,
  loadedUser: {status: null},
  createdUser: {status: null}
}

const getters = {
  userInfo: state => state.userInfo,
  users: state => state.users,
  loadedUser: state => state.loadedUser,
  createdUser: state => state.createdUser,
  pageNo: state => state.pageNo,
  pageCount: state => state.pageCount,
  hasNextPage: state => state.pageNo + 1 < state.pageCount,
  hasPrevPage: state => state.pageNo > 0
}

function commitChangePage(commit, r) {
  if (r) {
    commit('changePage', {
      page: _.get(r, 'data.page'),
      users: _.get(r, 'data._embedded.users', [])
    })
  }
}

function goToPage(pageNo, username, commit) {
  if (!username) {
    userSvc.getUsers(pageNo).then(r => {
      commitChangePage(commit, r)
    })
  } else {
    userSvc.searchByUsername(username, pageNo).then(r => {
      commitChangePage(commit, r)
    })
  }
}

const actions = {
  loadUserInfo({commit, state}) {
    if (!state.userInfo.username) {
      userSvc.getCurrent().then(
        resp => {
          if (resp) {
            commit('userInfo', resp.data)
          }

        })
    }

  },
  clearUserInfo({commit}) {
    commit('clearUserInfo')
  },
  loadUser({commit}, id) {
    userSvc.getUserForAdmin(id).then(r => {
      if (r) {
        commit('loadUser', r.data)
      }

    })
  },

  firstPage({commit}, username) {
    goToPage(0, username, commit)
  },
  nextPage({commit, getters}, username) {
    const nextPage = getters.pageNo + 1
    goToPage(nextPage, username, commit)
  },
  prevPage({commit, getters}, username) {
    const prevPage = getters.pageNo - 1
    goToPage(prevPage, username, commit)
  },
  createUser({commit}, s) {
    userSvc.createUser(s).then(r => {
      var link = _.get(r, 'headers.location')
      if (link) {
        userSvc.getUserFromLink(link)
          .then(s => {
            if (s) {
              commit('loadCreatedUser', s.data)
            }

          })
      }
    })
  },
  updateUser({commit}, s) {
    userSvc.updateUser(s).then(r => {
      // TODO: toaster?
    })
  },
  resetLoadedUser({commit}) {
    commit('resetLoadedUser')
  },
  resetCreatedUser({commit}) {
    commit('resetCreatedUser')
  }
}

const mutations = {
  userInfo(state, userInfo) {
    state.userInfo = userInfo
  },
  clearUserInfo(state) {
    state.userInfo = {}
  },
  changePage(state, {page, users}) {
    state.pageNo = page.number
    state.pageCount = page.totalPages
    state.users = users
  },
  loadUser(state, user) {
    state.loadedUser = user
  },
  loadCreatedUser(state, user) {
    state.createdUser = user
  },
  resetLoadedUser(state) {
    state.loadedUser = {status: null}
  },
  resetCreatedUser(state) {
    state.createdUser = {status: null}
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
