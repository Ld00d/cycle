import statusSvc from '@/services/status'
import _ from 'lodash'



const state = {
  siteStatuses: [],
  userStatuses: [],
  pageNo: 0,
  pageCount: 0,
  loadedStatus: {},
  loadedStatusType: null,
  createdStatus: {},
  createdStatusType: null
}

const getters = {
  siteStatuses: state => state.siteStatuses,
  userStatuses: state => state.userStatuses,
  loadedStatus: state => state.loadedStatus,
  loadedStatusType: state => state.loadedStatusType,
  createdStatus: state => state.createdStatus,
  pageNo: state => state.pageNo,
  pageCount: state => state.pageCount,
  hasNextPage: state => state.pageNo + 1 < state.pageCount,
  hasPrevPage: state => state.pageNo > 0
}

function commitChangePage(commit, type, r) {
  if (r) {
    commit('changePage', {
      page: _.get(r, 'data.page'),
      statuses: _.get(r, 'data._embedded.' + type + '-statuses', []),
      type: type
    })
  }
}

function goToPage(pageNo, type, name, commit) {
  if (!name) {
    statusSvc.getStatuses(type, pageNo).then(r => {
      commitChangePage(commit, type, r)
    })
  } else {
    statusSvc.searchByName(type, name, pageNo).then(r => {
      commitChangePage(commit, type, r)
    })
  }
}

const actions = {
  loadStatuses({commit}, type) {
    statusSvc.getStatuses(type).then(r => {
      if (r) {
        var statuses = _.get(r, 'data._embedded.' + type + '-statuses')
        commit('loadStatuses', {type, statuses})
      }
    })
  },
  loadStatus({commit}, {type, id}) {
    statusSvc.getStatus(type, id).then(r => {

      if (r) {
        var status = r.data
        commit('loadStatus', {type, status})
      }
    })
  },
  firstPage({commit}, {type, name}) {
    goToPage(0, type, name, commit)
  },
  nextPage({commit, getters}, {type, name}) {
    const nextPage = getters.pageNo + 1
    goToPage(nextPage, type, name, commit)
  },
  prevPage({commit, getters}, {type, name}) {
    const prevPage = getters.pageNo + 1
    goToPage(prevPage, type, name, commit)
  },
  createStatus({commit}, {type, status}) {
    statusSvc.createStatus(type, status).then(r => {
      var link = _.get(r, 'headers.location')
      if (link) {
        statusSvc.getStatusFromLink(link)
          .then(s => {
            if (s) {
              commit('loadCreatedStatus', {type, status: s.data})
            }
          })
      }
    }, e => {
      console.log({msg: 'error', err: e})
    })
  },
  updateStatus({commit}, s) {
    statusSvc.updateStatus(s).then(r => {
      //TODO: notify
    })
  },
  resetLoadedStatus({commit}) {
    commit('resetLoadedStatus')
  },
  resetCreatedStatus({commit}) {
    commit('resetCreatedStatus')
  }


}

const mutations = {
  loadStatuses(state, {type, statuses}) {
    state[type + 'Statuses'] = statuses
  },

  changePage(state, {page, statuses, type}) {
    state.pageNo = page.number
    state.pageCount = page.totalPages
    state[type + 'Statuses'] = statuses
  },

  loadStatus(state, {type, status}) {
    state.loadedStatus = status
    state.loadedStatusType = type
  },

  loadCreatedStatus(state, {type, status}) {
    state.createdStatus = status
    state.createdStatusType = type
  },

  resetLoadedStatus(state) {
    state.loadedStatus = {}
    state.loadedStatusType = null
  },

  resetCreatedStatus(state) {
    state.createdStatus = {}
    state.createdStatusType = null
  }


}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
