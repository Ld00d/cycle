import authSvc from '@/services/auth'
import events from '@/services/event'

const state = {
  loggedIn: false,
  token: null
}

function ensureState(state) {
  if (!state.loggedIn) {
    state.token = window.localStorage['auth_token']

    if (state.token) {
      state.loggedIn = true
    }
  }
}

const getters = {
  loggedIn: state => {
    ensureState(state)
    return state.loggedIn
  },
  authToken: state => {
    ensureState(state)
    return state.token
  }
}

const actions = {
  login({commit, dispatch}, {username, password}) {
    authSvc.login(username, password).then(
      resp => {
        if (resp.error) {
          let error = JSON.parse(resp.body)
          commit('loginFailure')
          dispatch('user/clearUserInfo', null, { root: true })
          events.$emit('show-error', error.message)


        } else {
          let token = resp.headers.authorization
          commit('login', {token})
          dispatch('user/loadUserInfo', null, { root: true })
        }

      },
      err => console.log(err))
      // TODO: handle error
  },
  logout({commit, dispatch}) {
    commit('logout')
    dispatch('user/clearUserInfo', null, { root: true })
  }
}

const mutations = {
  login(state, {token}) {
    state.token = token
    state.loggedIn = true
    window.localStorage['auth_token'] = token
  },
  logout(state) {
    state.token = null
    state.loggedIn = false
    delete window.localStorage['auth_token']
  },
  loginFailure(state) {
    state.token = null
    state.loggedIn = false
    delete window.localStorage['auth_token']
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
