import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import * as getters from './getters'
import auth from './modules/auth'
import user from './modules/user'
import ui from './modules/ui'
import site from './modules/site'
import status from './modules/status'

Vue.use(Vuex)

export default new Vuex.Store({
  actions,
  getters,
  modules: {
    auth,
    user,
    ui,
    site,
    status
  }
})
