// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import store from './store'
import App from './App'
import router from './router'
import VueSVGIcon from 'vue-svgicon'
import purecss from 'purecss'
import '@/assets/main.css'

import './filters.js'

Vue.use(VueSVGIcon)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App, purecss }
})
