import Vue from 'vue'
import Router from 'vue-router'
import Start from '@/components/Start'
import SystemAdmin from '@/components/hub/SystemAdmin'
import UserProfile from '@/components/UserProfile'
import SitesList from '@/components/hub/SitesList'
import SiteEdit from '@/components/hub/SiteEdit'
import UsersList from '@/components/hub/UsersList'
import UserEdit from '@/components/hub/UserEdit'
import StatusesList from '@/components/hub/StatusesList'
import StatusEdit from '@/components/hub/StatusEdit'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Start',
      component: Start
    },
    {
      path: '/system-admin',
      component: SystemAdmin,
      children: [
        {path: '', name: 'SystemAdmin', component: SitesList},
        {path: 'sites', name: 'SystemAdminSites', component: SitesList},
        {path: 'sites/new', name: 'SystemAdminSitesNew', component: SiteEdit},
        {path: 'sites/:id', name: 'SystemAdminSitesEdit', component: SiteEdit, props: true},
        {path: 'users', name: 'SystemAdminUsers', component: UsersList},
        {path: 'users/new', name: 'SystemAdminUsersNew', component: UserEdit},
        {path: 'users/:id', name: 'SystemAdminUsersEdit', component: UserEdit, props: true},
        {path: 'statuses/:type', name: 'SystemAdminStatuses', component: StatusesList, props: true},
        {path: 'statuses/:type/new', name: 'SystemAdminStatusesNew', component: StatusEdit, props: true},
        {path: 'statuses/:type/:id', name: 'SystemAdminStatusesEdit', component: StatusEdit, props: true}


      ]
    },
    {
      path: '/user-profile',
      name: 'UserProfile',
      component: UserProfile
    }
  ]
})
