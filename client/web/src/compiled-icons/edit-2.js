/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'edit-2': {
    width: 24,
    height: 24,
    viewBox: '0 0 24 24',
    data: '<path pid="0" d="M16 3l5 5L8 21H3v-5L16 3z"/>'
  }
})
