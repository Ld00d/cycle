/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'edit-3': {
    width: 24,
    height: 24,
    viewBox: '0 0 24 24',
    data: '<path pid="0" d="M14 2l4 4L7 17H3v-4L14 2zM3 22h18"/>'
  }
})
