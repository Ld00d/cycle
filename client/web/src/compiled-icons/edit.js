/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'edit': {
    width: 24,
    height: 24,
    viewBox: '0 0 24 24',
    data: '<path pid="0" d="M20 14.66V20a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h5.34"/><path pid="1" d="M18 2l4 4-10 10H8v-4L18 2z"/>'
  }
})
