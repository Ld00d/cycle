/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'slack': {
    width: 24,
    height: 24,
    viewBox: '0 0 24 24',
    data: '<path pid="0" d="M22.08 9C19.81 1.41 16.54-.35 9 1.92S-.35 7.46 1.92 15 7.46 24.35 15 22.08 24.35 16.54 22.08 9zM12.57 5.99l3.58 10.4M7.85 7.61l3.58 10.4M16.39 7.85l-10.4 3.58M18.01 12.57l-10.4 3.58"/>'
  }
})
