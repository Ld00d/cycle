/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'list': {
    width: 24,
    height: 24,
    viewBox: '0 0 24 24',
    data: '<path pid="0" d="M8 6h13M8 12h13M8 18h13M3 18"/>'
  }
})
